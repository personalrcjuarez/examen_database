/* Muestra el número de ventas de cada producto ordenado de mayor a menor venta */

SELECT v.producto,p.nombre, count(v.cantidad) as numero_ventas FROM venta as v
left join productos as p on p.producto = v.producto
group by v.producto order by numero_ventas desc

/*
 Informe completo de ventas que muestra el cajero quien realizó la venta de un determinado producto, con su respectivo precio y cantidad vendida
así como el piso y la fecha donde fue efectuada la venta
*/

SELECT c.nom_apels,p.nombre, v.cantidad, p.precio, m.piso, v.fecha FROM venta as v
left join productos as p on p.producto = v.producto
left join cajeros as c on c.cajero = v.cajero
left join maquinas_registradoras as m on v.maquina = m.maquina

/* Ventas totales realizado en cada piso */

SELECT  m.piso, sum(v.cantidad*p.precio) ventas_totales FROM venta as v
left join productos as p on p.producto = v.producto
left join maquinas_registradoras as m on v.maquina = m.maquina
group by v.maquina

/* Total de ventas de cada cajero, mostrando su código y nombre */

SELECT c.cajero, c.nom_apels, sum(v.cantidad*p.precio) FROM venta as v
left join productos as p on p.producto = v.producto
left join cajeros as c on c.cajero = v.cajero
group by c.cajero

/* Cajeros que han vendido menos de 5,000 por piso*/ 

SELECT c.cajero, c.nom_apels,m.piso,sum(v.cantidad*p.precio) total_ventas FROM venta as v
left join productos as p on p.producto = v.producto
left join cajeros as c on c.cajero = v.cajero
left join maquinas_registradoras as m on v.maquina = m.maquina
group by c.cajero, m.piso 
HAVING total_ventas <= 5000